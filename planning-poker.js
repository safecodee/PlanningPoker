Rooms = new Mongo.Collection("rooms");
CardsForEstimation = new Mongo.Collection("cardsForEstimation");


if (Meteor.isClient) {
    Meteor.subscribe("rooms");
    Meteor.subscribe("cardsForEstimation");
    var currentVersion = 5;

    var _userInfo = {userId: uuid.v4(), roomId: null, userName: null, isShowResult: false, version: currentVersion};

    if (!Session.get("userInfo")) {
        Session.setPersistent("userInfo", _userInfo);
    }

    if (Session.get("userInfo") && (Session.get("userInfo").version != currentVersion)) {
        Session.setPersistent("userInfo", _userInfo);
    }

    Template.body.helpers(   {
        isInRoom: function () {
            var roomId = Session.get("userInfo").roomId;
            var result = !!roomId && !!Rooms.findOne({"roomId": roomId});
            return result;
        }
    });

    Template.vote.helpers({
        getRoomNumber: function () {
            return Session.get("userInfo").roomId;
        },
        isOwnerRoom: function () {
            return !!Rooms.findOne({roomId: Session.get("userInfo").roomId, ownerId: Session.get("userInfo").userId});
        }
    });

    Template.userVotes.helpers({
        getCountUsers: function () {
            return Rooms.findOne({roomId: Session.get("userInfo").roomId});
        },
        isShowResult: function () {
            return Rooms.findOne({roomId: Session.get("userInfo").roomId}).isShowResult;
        }
    });

    Template.vote.events({
        "click .quit": function () {
            Meteor.call("userQuit", Session.get("userInfo"), function () {
                var userInfo = Session.get("userInfo");
                userInfo["roomId"] = null;
                Session.setPersistent("userInfo", userInfo);
            });
        }
    });

    Template.cardsForEstimation.helpers({
        cards: function () {
            return CardsForEstimation.findOne();
        },
        isChooseCard: function (value) {
            var userInfo = Session.get("userInfo");
            var room = Rooms.findOne({roomId: userInfo.roomId});
            var result = false;
            room.voters.forEach(function (voter) {
                if ((voter.userId == Session.get("userInfo").userId) && (value == voter.vote)) {
                    result = true;
                }
            });

            return result;
        }
    });

    function vote(event) {
        var vote = event.target.innerHTML;
        var userInfo = Session.get("userInfo");

        Meteor.call("vote", {
            roomId: userInfo.roomId,
            userId: userInfo.userId,
            vote: vote
        });
    }

    Template.cardsForEstimation.events({
        "click .cardForEstimate": function (event) {
            vote(event);
        },
        "touchstart .cardForEstimate": function (event) {
            vote(event);
        }
    });

    Template.startManageButtons.events({
        "click .createRoom": function () {
            Meteor.call("createRoom", Session.get("userInfo"), function (err, result) {
                var userInfo = Session.get("userInfo");
                userInfo.roomId = result;
                Session.setPersistent("userInfo", userInfo);
            });
        },
        "submit .joinRoom": function (event) {
            var roomId = event.target.roomNumber.value;
            if (!roomId && !Number.isInteger(roomId)) {
                return;
            }
            Meteor.call("joinRoom", roomId, Session.get("userInfo").userId, function (err, roomId) {
                var userInfo = Session.get("userInfo");
                userInfo.roomId = roomId;
                Session.setPersistent("userInfo", userInfo);
            });
            return false;
        }
    });

    Template.buttonForManageRoom.events({
        "click .reset": function () {
            Meteor.call("reset", Session.get("userInfo").roomId, function () {
                var userInfo = Session.get("userInfo");
                userInfo.isShowResult = false;
                Session.setPersistent(userInfo);
            });
        },
        "click .showResult": function () {
            Meteor.call("showResult", Session.get("userInfo").roomId);
        }
    });
}

if (Meteor.isServer) {
    var schedule = later.parse.recur().every(5).minute();
    var deletedNotActiveRooms = new ScheduledTask(schedule, function () {
        Meteor.call("removeUnactiveRooms");
    });

    Meteor.startup(function () {
        Rooms.remove({});
        CardsForEstimation.remove({});
        CardsForEstimation.insert({type: 'normal', values: [0, 1, 2, 3, 5, 8, 13, 20, 40, 100]});

        deletedNotActiveRooms.start();
    });

    Meteor.publish("rooms", function () {
        return Rooms.find();
    });
    Meteor.publish("cardsForEstimation", function () {
        return CardsForEstimation.find();
    });
}

Meteor.methods({
    removeUnactiveRooms: function () {
        var boundDate = new Date((new Date()).getTime() - (15 * 60 * 1000));
        Rooms.remove({lastActiveTime: {$lte: boundDate}});
    },

    createRoom: function (userInfo) {
        for (; ;) {
            var roomId = Math.floor(Math.random() * 10000);
            if (!Rooms.findOne({roomId: roomId})) {
                Rooms.insert({
                    roomId: roomId,
                    ownerId: userInfo.userId,
                    lastActiveTime: new Date(),
                    voters: [{userId: userInfo.userId}]
                });
                return roomId;
            }
        }
    },

    joinRoom: function (roomId, userId) {
        var roomIdInt = parseInt(roomId);
        Rooms.update({roomId: roomIdInt}, {$push: {voters: {"userId": userId}}});
        return roomIdInt;
    },

    userQuit: function (userInfo) {
        Rooms.update({roomId: userInfo.roomId}, {$pull: {voters: {userId: userInfo.userId}}});
    },

    vote: function (voteInfo) {
        var roomId = parseInt(voteInfo.roomId);
        var vote = voteInfo.vote.trim();
        var result = Rooms.findOne({roomId: roomId}).isShowResult;

        Meteor.call("askNeedShowResult", roomId, function (err, result) {
            if (!result) {
                Rooms.update({roomId: roomId, "voters.userId": voteInfo.userId}, {$set: {"voters.$.vote": vote}});
                Rooms.update({roomId: roomId}, {$set: {"lastActiveTime": new Date()}});
                Meteor.call("askNeedShowResult", roomId);
            }
        });

        return !result;
    },

    reset: function (roomId) {
        var roomIdInt = parseInt(roomId);

        for (; ;) {
            var room = Rooms.findOne({roomId: roomIdInt, "voters.vote": {$exists: true}});
            if (!room) {
                break;
            }
            Rooms.update({
                roomId: roomIdInt,
                "voters.vote": {$exists: true}
            }, {$unset: {"voters.$.vote": null}}, {multi: true});
        }

        Rooms.update({
            roomId: roomIdInt, "isShowResult": {$exists: true}
        }, {$set: {"isShowResult": false}});
    },

    showResult: function (roomId) {
        var roomIdInt = parseInt(roomId);

        //There is voters
        if (Rooms.findOne({roomId: roomIdInt, "voters.vote": {$exists: true}})) {
            Rooms.update({roomId: roomIdInt}, {$set: {isShowResult: true}});
        }
    },

    askNeedShowResult: function (roomId) {
        var roomIdInt = parseInt(roomId);

        var room = Rooms.findOne({roomId: roomIdInt});
        if (room && ("isShowResult" in room) && (room["isShowResult"])) {
            return true;
        }
        var totalUsers = room ? room.voters.length : 0;
        var alreadyVote = 0;
        room.voters.forEach(function (voter) {
            if (voter.vote) {
                alreadyVote++
            }
        });
        var result = (totalUsers == alreadyVote);
        if (result) {
            Meteor.call("showResult", roomIdInt);
        }
        return result;
    }
});
